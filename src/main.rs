use futures::executor::block_on;
use check_if_email_exists::{check_email, CheckEmailInput};

async fn check(email:String) {
    // Let's say we want to test the deliverability of someone@gmail.com.
    let input = CheckEmailInput::new(vec![email.into()]);

    // Optionally, we can also tweak the configuration parameters used in the
    // verification.
    /*
    input
        .from_email("b19kiit@gmail.com".into()) // Used in the `MAIL FROM:` command
        .hello_name("gmail.com".into()); // Used in the `EHLO` command
    */

    // Verify this input, using async/await syntax.
    let result = check_email(&input).await;

    // `result` is a `Vec<CheckEmailOutput>`, where the CheckEmailOutput
    // struct contains all information about our email.
    println!("{:?}", result);
}

fn main() {
    let c_fut1 = check(String::from("b19kiit@gmail.com"));
    block_on(c_fut1);
    let c_fut2 = check(String::from("b1o8h08hwfh0hj9kiit@gmail.com"));
    block_on(c_fut2);
    let c_fut3 = check(String::from("1706340@kiit.ac.in"));
    block_on(c_fut3);
    let c_fut4 = check(String::from("22706459@kiit.ac.in"));
    block_on(c_fut4);
    let c_fut5 = check(String::from("rishavbhowmik@outlook.com"));
    block_on(c_fut5);
    let c_fut6 = check(String::from("b19ksdweiit@outlook.com"));
    block_on(c_fut6);
    let c_fut7 = check(String::from("raja.subramanian@cybernetyx.com"));
    block_on(c_fut7);
    let c_fut8 = check(String::from("raja.raja.raja.raja.raja@cybernetyx.com"));
    block_on(c_fut8);
}
//raja.subramanian@cybernetyx.com <raja.subramanian@cybernetyx.com>;