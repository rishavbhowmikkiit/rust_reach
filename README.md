### Follow these steps to Check if port 25 works

- Download executable file

```
curl https://bitbucket.org/rishavbhowmikkiit/rust_reach/raw/d26da23764dfb4b4a15c2bbec5a1f17500c26479/reach > reach.o
```

*This may take some time, file size is 111MB*

- Set `reach.o` as an executable file

```
chmod +x ./reach.o
```

- Run `./reach.o`

```
./reach.o
```

Incase System return Permission denied

```
sudo ./reach.o
```

- Post execution following result would be printed

```
[CheckEmailOutput { input: "b12342349kiit@gmail.com", is_reachable: Unknown, misc: Ok(MiscDetails { is_disposable: false, is_role_account: false }), mx: Ok(MxDetails { lookup: Ok(MxLookup(Lookup { query: Query { name: Name { is_fqdn: false, labels: [gmail, com] }, query_type: MX, query_class: IN }, records: [Record { name_labels: Name { is_fqdn: true, labels: [gmail, com] }, rr_type: MX, dns_class: IN, ttl: 2931, rdata: MX(MX { preference: 30, exchange: Name { is_fqdn: true, labels: [alt3, gmail-smtp-in, l, google, com] } }) }, Record { name_labels: Name { is_fqdn: true, labels: [gmail, com] }, rr_type: MX, dns_class: IN, ttl: 2931, rdata: MX(MX { preference: 20, exchange: Name { is_fqdn: true, labels: [alt2, gmail-smtp-in, l, google, com] } }) }, Record { name_labels: Name { is_fqdn: true, labels: [gmail, com] }, rr_type: MX, dns_class: IN, ttl: 2931, rdata: MX(MX { preference: 5, exchange: Name { is_fqdn: true, labels: [gmail-smtp-in, l, google, com] } }) }, Record { name_labels: Name { is_fqdn: true, labels: [gmail, com] }, rr_type: MX, dns_class: IN, ttl: 2931, rdata: MX(MX { preference: 10, exchange: Name { is_fqdn: true, labels: [alt1, gmail-smtp-in, l, google, com] } }) }, Record { name_labels: Name { is_fqdn: true, labels: [gmail, com] }, rr_type: MX, dns_class: IN, ttl: 2931, rdata: MX(MX { preference: 40, exchange: Name { is_fqdn: true, labels: [alt4, gmail-smtp-in, l, google, com] } }) }], valid_until: Instant { tv_sec: 2310197, tv_nsec: 20397636 } })) }), smtp: Err(SmtpError(Client("Connection closed"))), syntax: SyntaxDetails { address: Some(EmailAddress("b12342349kiit@gmail.com")), domain: "gmail.com", is_valid_syntax: true, username: "b12342349kiit" } }]
```

*This may take some time too, 60 - 120 seconds depending on network*

- We need to check if `CheckEmailOutput.is_reachable` of the result is not `Unknown`